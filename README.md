# Draft for AI/ML internal training

> Note: This repository contains notebooks on  how `hands-on` introduction of AI & ML based on my personal experience in the whole ML lifecycle. These notes represent the views of the authors, not those of HT Consulting Sdn Bhd. These notes were designed for internal use only, not as reference material. The notes are intended as a summary of a given area only and should not be a substitute for specialist advice. 
